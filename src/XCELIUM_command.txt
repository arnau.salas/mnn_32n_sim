command:

xrun +access+r -v93 -messages maxmin_top.vhd tb_maxmin_top_verilog.v -input xrun.tcl -makelib work  MyTypes.vhd maxmin_net_pkg.vhd maxmin_net.vhd add_accum.vhd apc.vhd clip_reg.vhd lfsr_nbits.vhd neurona_maxmin.vhd neurona_sc.vhd pcounter.vhd sinc_net.vhd unip2bip.vhd weights_hardwired.vhd maxmin_layer.vhd maxmin_net.vhd -endlib;