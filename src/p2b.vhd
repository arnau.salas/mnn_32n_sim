
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use IEEE.std_logic_unsigned.all;

-- -- Template Isntantiation
-- inst_p2b_x0 : entity work.p2b
-- generic map(GNRC_BIT_PRECISSION,GNRC_OUT_FORMAT)
-- port map (clk, rst, '1', portIn_bs_tick, portIn_x_sc(0), p2b_x0_bin);

entity p2b is

	generic (
		-- Tamaño en bits del resultado de la convercion
		GNRC_BIT_STREAM_WIDTH	: integer := 16;
        GNRC_OUT_FORMAT : integer := 0 -- 0 unipolar, 1	: bipolar
	);
	port
	(
		-- Clock del sistema
		clk	  : in std_logic;
		rst		  : in std_logic;
		-- enable el modulo cuando esta en '1'
		portIn_en		  : in std_logic;
        -- cuando bitStream se complete
		portIn_bs_tick  : in std_logic;
		-- Entrada estocastica para ser convertida
		portIn_sc	  : in std_logic;		
		-- Resultado de la conversion 
		portOut_bin		  : out std_logic_vector(GNRC_BIT_STREAM_WIDTH-1 downto 0)
		
	);

end p2b;

architecture rtl of p2b is	
    signal cont_pulses : integer range 0 to 2**GNRC_BIT_STREAM_WIDTH -1 := 0;
    signal result : integer range -2**(GNRC_BIT_STREAM_WIDTH -1) to 2**(GNRC_BIT_STREAM_WIDTH -1)-1 := 0;
	signal portOut_bin_s : std_logic_vector(GNRC_BIT_STREAM_WIDTH-1 downto 0);

begin

	process (clk)		
	begin
		if (rising_edge(clk)) then
			if rst = '1' then
				cont_pulses <= 0;
                result <= 0;
			elsif portIn_en = '1' then
				-- si ya llego a su final				
				if portIn_bs_tick='1' then
                    cont_pulses <= 0; 
					
					result <= cont_pulses - 2**(GNRC_BIT_STREAM_WIDTH-1);
					
					-- result <= cont_pulses;
					portOut_bin_s <= std_logic_vector(to_unsigned(cont_pulses, portOut_bin_s'length));
                elsif PortIn_sc = '1' then
					cont_pulses <= cont_pulses + 1;		
				end if;
			end if;
		end if;
	end process;

    portOut_bin <= std_logic_vector(to_signed(result, portOut_bin'length));

end rtl;
