dumptcf -output tb/dump.tcf -dumpwireasnet -overwrite
call vcdfile testpatterns.vcd -t ns
database -open -vcd vcddb -default -timescale ns
probe -create -all -variables
run 1000 ns
dumptcf -end

exit
